<?php

namespace App\Controller;

use App\Entity\Pharmacie;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PharmacieController extends AbstractController
{
    /**
     * @Route("/pharma", name="pharmacie")
     */
    public function index(EntityManagerInterface $em): Response
    {
        $repo = $em->getRepository('App\Entity\Pharmacie');

        
        return $this->json([
            
            $repo->findAll()
        ]);
    }
}
